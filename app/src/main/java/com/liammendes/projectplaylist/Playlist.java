package com.liammendes.projectplaylist;

import java.util.Map;

/**
 * Created by liammendes on 09/06/2017.
 */

public class Playlist {

    public String Date;
    public String ImgPreview;
    public String PlaylistName;
    public String Source;
    public String TimeCreated;
    public String Timestamp;
    public String PlaylistUUID;
    public String UserCreatedPLUUID;
    public String VideoId;

    //Likes
    public String HRNumberL;
    public String NumberL; // Stored the Number in a string to detect if we need a int, BigInteger or Long

    //Views
    public String HRNumberV;
    public String NumberV;

    public String PDisplayName;

    public Playlist(Map entry) {
        Date = ((String)entry.get("Date"));
        ImgPreview = ((String)entry.get("ImgPreview"));
        PlaylistName = ((String)entry.get("PlaylistName"));
        Source = ((String)entry.get("Source"));
        TimeCreated = ((String)entry.get("Time"));
        Timestamp = ((String)entry.get("Timestamp"));
        PlaylistUUID = ((String)entry.get("UUID"));
        UserCreatedPLUUID = ((String)entry.get("UserUUID"));
        VideoId = ((String)entry.get("videoId"));

        Map Likes = ((Map)entry.get("Likes"));
        Map Views = ((Map)entry.get("Views"));

        HRNumberL = ((String)Likes.get("HRNumber"));
        NumberL = ((Long)Likes.get("Number")).toString();

        HRNumberV = ((String)Views.get("HRNumber"));
        NumberV = ((Long)Views.get("Number")).toString();

    }

    public void setUserDisplayName(String DisplayName){
        PDisplayName = DisplayName;
    }

    public String getUserDisplayName() { return PDisplayName;}
}
