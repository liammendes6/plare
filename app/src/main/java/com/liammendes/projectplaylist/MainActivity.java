package com.liammendes.projectplaylist;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.makeramen.roundedimageview.RoundedImageView;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.liammendes.projectplaylist.MainActivity.ContentType.DISCOVER;
import static com.liammendes.projectplaylist.MainActivity.ContentType.HOME;
import static com.liammendes.projectplaylist.MainActivity.ContentType.MYPROFILE;

public class MainActivity extends AppCompatActivity{

    private boolean signedUp;

    Playlist[] playlistsStr = {};
    Playlist[] playlistsStrings;

    Long MaxPlaylists;
    Long LoadPage;// Default Value

    private ContentType currentNav;
    private ProgressDialog mDialog;
    private View inflated;

    private int length = 0;
    private final MainActivity mainActivity = this;
    private NavigationView navView;
    private GoogleSignInAccount gsia;


    private class UploadToFirebaseTask extends AsyncTask<Object, Integer, byte[]> {
        protected byte[] doInBackground(final Object... objects) {
                    URL url = null;
                    try {
                        url = new URL(objects[0].toString());
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    }
                    URLConnection ucon = null;
                    try {
                        ucon = url.openConnection();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } try {
                        is = ucon.getInputStream();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    byte[] propi = null;
                    try {
                        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

                        int nRead;
                        byte[] data = new byte[16384];

                        while ((nRead = is.read(data, 0, data.length)) != -1) {
                            buffer.write(data, 0, nRead);
                        }

                        buffer.flush();
                        propi = data;

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    byte[] pubData = propi;
            /**int count = urls.length;
            long totalSize = 0;
            for (int i = 0; i < count; i++) {
                totalSize += Downloader.downloadFile(urls[i]);
                publishProgress((int) ((i / (float) count) * 100));
                // Escape early if cancel() is called
                if (isCancelled()) break;
            }
            return totalSize;**/
            return pubData;
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(Long result) {
        }
    }
    private TextView mTextMessage;
    public boolean signedIn = false;
    private static final String TAG = "MainActivity";
    private NavigationView.OnNavigationItemSelectedListener oNISL = null;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = null;
    private InputStream is;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        init();
        super.onCreate(savedInstanceState);
        loadInitialContent(currentNav);
        setUpGoogleAccount(getIntent().getExtras());
        if(checkNotSignedIn() == true){
            return;
        }
        startSignUpFlow();
    }

    private void startSignUpFlow() {
        DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference("Users").child(String.valueOf(new BigInteger(gsia.getEmail().substring(0, 15).getBytes())));
        final GoogleSignInAccount finalGsia = gsia;
        dbRef.child("signedUp").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e(TAG, String.valueOf(dataSnapshot.exists()));
                if(dataSnapshot.exists() == false){ signedUp = false; return;}
                if ((boolean)dataSnapshot.getValue() == true) {
                    signedUp = true;
                } else {
                    signedUp = false;
                }
                Log.e(TAG, "signedUp: " + String.valueOf(signedUp));
                if (!signedUp) {
                    signUp(finalGsia);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getDetails());
            }
        });
    }

    private boolean checkNotSignedIn() {
        if(gsia == null){
            signedIn = false;
            navView.getMenu().clear();
            navView.inflateMenu(R.menu.activity_main2_drawer);
            navView.setNavigationItemSelectedListener(oNISL);
            return true;
        } else {
             return false;
        }
    }

    private void setUpGoogleAccount(Bundle extras) {
        gsia = null;
        if(extras == null){
            //No User has been defined.
            noUserDefined();
            return;
        } else {
        //User has been signedIn
            setUpNavView();
            loadDataFromGSIA();
            setUpUserAccount();
        }
    }

    private void setUpUserAccount() {
        FirebaseStorage storage = FirebaseStorage.getInstance();
        String useruuid = "";
        final StorageReference sr = storage.getReference().child("Users").child("propi/" + useruuid + ".png");
        final Uri personPhoto = gsia.getPhotoUrl();
        ProgressDialog pd = ProgressDialog.show(MainActivity.this, "",
                "Uploading Profile Picture from Google to Plare Storage...", true);
        pd.show();
        UploadToFirebaseTask utFT = new UploadToFirebaseTask();
        utFT.execute(personPhoto, sr);
        pd.dismiss();

        signedIn = true;
    }

    private void setUpNavView() {
        navView.getMenu().clear();
        navView.inflateMenu(R.menu.activity_main_drawer);
        navView.setNavigationItemSelectedListener(oNISL);
    }

    private void loadDataFromGSIA() {
        gsia = (getIntent().getExtras().getParcelable("LOGIN_ACCOUNT"));
        // Update the profile picture onCreate of MainActivity
        // To always stay up to date with the users Profile Picture
        View hView = navView.getHeaderView(0);
        TextView displayName = (TextView) hView.findViewById(R.id.UserDisplayName);
        TextView handle = (TextView) hView.findViewById(R.id.UserHandle);
        handle.setText(gsia.getEmail());
        RoundedImageView iv = (RoundedImageView) hView.findViewById(R.id.imageView);
        iv.setCornerRadius(64f);
        Picasso.with(this).load(gsia.getPhotoUrl()).into(iv);
        displayName.setText(gsia.getDisplayName());
    }

    private void noUserDefined() {
        gsia = null;
        signedIn = false;
        navView.getMenu().clear();
        navView.inflateMenu(R.menu.activity_main2_drawer);
        navView.setNavigationItemSelectedListener(oNISL);
    }

    private void init() {
        initVariables();
        setUpGUI();
    }

    private void setUpGUI() {
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mTextMessage = (TextView) findViewById(R.id.message);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.container);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navView = (NavigationView) findViewById(R.id.nav_view);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        navigation.getMenu().getItem(0).setChecked(true);
        if(navigation.getMenu().getItem(0).isChecked() == true){
            currentNav = ContentType.HOME;
        } else if(navigation.getMenu().getItem(1).isChecked() == true){
            currentNav = ContentType.DISCOVER;
        } else if(navigation.getMenu().getItem(2).isChecked() == true){
            currentNav = ContentType.MYPROFILE;
        }
    }

    private void initVariables() {
        mDialog = new ProgressDialog(this);
        oNISL = new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.nav_listshare || item.getItemId() == R.id.nav_listshare2) {
                    startListShare();
                    return true;
                } else if (item.getItemId() == R.id.nav_importsignin) {
                    startImportPlaylists();
                    return true;
                } else if (item.getItemId() == R.id.nav_importsignin2) {
                    startSignIn();
                    return true;
                }
                else if (item.getItemId() == R.id.nav_logoutshare) {
                    startLogout();
                    return true;
                } else if (item.getItemId() == R.id.nav_logoutshare2) {
                    startShare();
                    return true;

                } else if (item.getItemId() == R.id.nav_settings || item.getItemId() == R.id.nav_settings2) {
                    startSettings();
                    return true;
                }

                DrawerLayout drawer = (DrawerLayout) findViewById(R.id.container);
                drawer.closeDrawer(GravityCompat.START);
                return false;
            }

        };
        mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        mTextMessage.setText(R.string.title_home);
                        currentNav = HOME;
                        if(currentNav == HOME){
                            break;
                        } else {
                            loadContent(ContentType.HOME);
                        }
                        return true;
                    case R.id.navigation_discover:
                        mTextMessage.setText(R.string.title_discover);
                        currentNav = DISCOVER;
                        if(currentNav == DISCOVER){
                            break;
                        } else {
                            loadContent(ContentType.DISCOVER);
                        }
                        return true;
                    case R.id.navigation_myprofile:
                        mTextMessage.setText(R.string.title_myprofile);
                        currentNav = MYPROFILE;
                        if(currentNav == MYPROFILE){
                            break;
                        } else {
                            loadContent(ContentType.MYPROFILE);
                        }
                        return true;
                }
                return false;
            }

        };
    }

    private void startListShare() {
        Log.e(TAG, "Method startSignIn Called");
        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(i);
    }
    private void startSettings() {
        Log.e(TAG, "Method startSignIn Called");
        Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
        i.putExtra("LOGIN_ACCOUNT", (getIntent().getExtras().getParcelable("LOGIN_ACCOUNT")));
        startActivity(i);
    }
    private void loadContent(ContentType currentNav) {
        switch (currentNav){
            case HOME:
                loadContentHome();
            case DISCOVER:
                loadContentDiscover();
            case MYPROFILE:
                loadContentMyProfile();
        }
    }
    private void loadContentMyProfile() {
        LinearLayout ll = (LinearLayout) findViewById(R.id.ContentPanel);
    }
    private void loadContentDiscover() {
        LinearLayout ll = (LinearLayout) findViewById(R.id.ContentPanel);
    }
    private void loadContentHome() {
        final FirebaseDatabase firDB = FirebaseDatabase.getInstance();
        firDB.getReference("Home").child("MaxPlaylists").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e(TAG, "MaxPlaylists B = " + MaxPlaylists);
                MaxPlaylists = (Long) dataSnapshot.getValue();
                Log.e(TAG, "MaxPlaylists A = " + MaxPlaylists);
                playlistsStr = new Playlist[MaxPlaylists.intValue()];// Temporary Value, This is how many playlists we have in the database ATM
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
        firDB.getReference("Home").child("LoadPage").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.e(TAG, "LoadPage B = " + LoadPage);
                LoadPage = (Long) dataSnapshot.getValue();
                Log.e(TAG, "LoadPage A = " + LoadPage);
                firDB.getReference("Home").child("PlaylistFeat").limitToFirst(LoadPage.intValue()).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Map<String,Object> playlists = (Map<String,Object>) dataSnapshot.getValue();
                        for(Map.Entry<String,Object> entry : playlists.entrySet()){
                            if(length != LoadPage) {
                                length++;
                                Playlist pl = new Playlist((Map) entry.getValue());
                                playlistsStr[(length)-1] = pl;
                            } else return;
                        }
                        RenderContentHome();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.e(TAG, databaseError.getMessage());
                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e(TAG, databaseError.getMessage());
            }
        });
    }
    private void RenderContentHome(){
        while(playlistsStr == null || MaxPlaylists == null){

        }

        List<Playlist> list = new ArrayList<Playlist>();

        for(Playlist s : playlistsStr) {
            if(s != null) {
                list.add(s);
            }
        }

        playlistsStrings = list.toArray(new Playlist[list.size()]);

        LinearLayout ll = (LinearLayout) findViewById(R.id.ContentPanel);
        final LinearLayout subView = (LinearLayout) ll.findViewById(R.id.BNavBarContent);
        final MainActivity mActivity = mainActivity;
        for(final Playlist playl : playlistsStrings){
            FirebaseDatabase.getInstance().getReference("Users").child(playl.UserCreatedPLUUID).child("Name").addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    playl.setUserDisplayName((String) dataSnapshot.getValue());
                    for(final Playlist playlist : playlistsStrings) {
                        Log.e(TAG, playlist.getUserDisplayName() + " = playlist.getUserDisplayName() ; " + playlist);

                        View inflatedPelement = getLayoutInflater().inflate(R.layout.playlist_element, subView);

                        CardView cv = (CardView) findViewById(R.id.cv);
                        cv.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mActivity.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.youtube.com/watch?v=" + playlist.VideoId)));
                            }
                        });

                        //ll.addView(View.inflate(ll.getContext(), R.layout.playlist_element, null));
                        TextView textView = (TextView) inflatedPelement.findViewById(R.id.PlaylistTitle);
                        textView.setTextColor(Color.WHITE);
                        textView.setText(playlist.PlaylistName);

                        ImageView imageView = (ImageView) (inflatedPelement.findViewById(R.id.cv).findViewById(R.id.iv));
                        Log.e(TAG, String.valueOf(inflatedPelement.findViewById(R.id.cv).getWidth()));
                        Picasso.with(imageView.getContext())
                                .load(playlist.ImgPreview)
                                .resize(getWindowManager().getDefaultDisplay().getWidth() - ((int) getResources().getDimension(R.dimen.size_marginlr) * 2), 0)
                                .into(imageView);

                        TextView tv = (TextView) findViewById(R.id.likesViews);
                        tv.setText(playlist.HRNumberL + " • " + playlist.HRNumberV);

                        TextView tv2 = (TextView) findViewById(R.id.creator);
                        //tv2.setText("created by " + playlist.getUserDisplayName());

                        Button likeButton = (Button) findViewById(R.id.button_like);
                        likeButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Like not Supported Yet
                            }
                        });
                        Button shareButton = (Button) findViewById(R.id.button_share);
                        likeButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                //Like not Supported Yet
                            }
                        });
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, databaseError.getMessage());
                }
            });
        }
        mDialog.dismiss();
    }
    public int dp2px(int dp) {
        WindowManager wm = (WindowManager) this.getBaseContext()
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics displaymetrics = new DisplayMetrics();
        display.getMetrics(displaymetrics);
        return (int) (dp * displaymetrics.density + 0.5f);
    }
    private void signUp(GoogleSignInAccount gsia) {
        FirebaseDatabase fd = FirebaseDatabase.getInstance();
        final DatabaseReference UserProfile = fd.getReference("Users").child(getUUID(gsia.getEmail()).toString());
        UserProfile.child("Name").setValue(gsia.getDisplayName());
        UserProfile.child("handle").setValue("@");
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater inflater = MainActivity.this.getLayoutInflater();
        builder.setView(inflated = inflater.inflate(R.layout.dialog_chooseusername, null))
                .setPositiveButton(R.string.signin, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        UserProfile.child("username").setValue(((TextView)inflated.findViewById(R.id.username)).getText().toString());
                        UserProfile.child("signedUp").setValue(true);
                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });
        
        builder.create().show();
    }
    private BigInteger getUUID(String email){return new BigInteger(email.substring(0, Math.min(email.length(), 15)).getBytes());}
    private boolean signedUp(Object gsia, String sIprovider, boolean usernameExists, boolean UUIDExists) {
        final boolean[] userExists = new boolean[1];GoogleSignInAccount signInObject = (GoogleSignInAccount) gsia;
            if(signInObject.getEmail().length() < 15) {Snackbar.make((DrawerLayout) findViewById(R.id.container), "Can't check your status for Plare. Your E-mail needs to have at least 15 characters to generate a UUID for your profile. Try a different account or Try again later.", Snackbar.LENGTH_SHORT).show();userExists[0] = false;}if(!usernameExists || !UUIDExists){userExists[0] = false;} else {userExists[0] = true;}
        return userExists[0];
    }
    @Override
    public void onBackPressed() {
        if (((DrawerLayout) findViewById(R.id.container)).isDrawerOpen(GravityCompat.START)) {
            ((DrawerLayout) findViewById(R.id.container)).closeDrawer(GravityCompat.START);
        } else {
            startLogout();
            super.onBackPressed();
        }
    }
    private void startImportPlaylists() {
    }
    private void startShare() {
        Intent sendIntent = new Intent();sendIntent.setAction(Intent.ACTION_SEND);sendIntent.putExtra(Intent.EXTRA_TEXT, "Hey, Play some music with me with Plare!");sendIntent.setType("text/plain");startActivity(sendIntent);
    }
    private void startSignIn() {Intent i = new Intent(getApplicationContext(), LoginActivity.class);startActivity(i);}
    private void startLogout() {Intent i = new Intent(getApplicationContext(), MainActivity.class);// We are not passing any parameters to the intent
        startActivity(i);}
    private void loadInitialContent(ContentType cNav){
        mDialog.setMessage("Please wait...");
        mDialog.setCancelable(false);
        mDialog.show();
        loadContent(cNav);
    }
    public enum ContentType {HOME, DISCOVER, MYPROFILE}
}
